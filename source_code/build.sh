set -x
rm -f game_script
if [ ! -f ape.o ]; then cc -c -g -O2 ../source_code_for_redgpu_version/ape.c; fi
if [ ! -f stdpack.o ]; then cc -c -g -O2 src/r-lyeh/stdpack.c/stdpack.c; fi
if [ ! -f droid_sans_mono.o ]; then cc -c -O2 ../source_code_for_redgpu_version/droid_sans_mono.c; fi
if [ ! -f math_finite.o ]; then cc -c -g -O2 math_finite.c; fi
c++ -g -O2 -std=c++14 main.cpp gamescript_imgui_custom_input_text_multiline.cpp ape.o stdpack.o droid_sans_mono.o math_finite.o -I../source_code_for_redgpu_version -Iinclude -Iinclude/embree -Iinclude/embree/renderer -pthread -lm -ldl *.so libcurl.so.4 libidn.so.11 liblber-2.4.so.2 libldap_r-2.4.so.2 libgssapi.so.3 libheimntlm.so.0 libkrb5.so.26 libasn1.so.8 libhcrypto.so.4 libroken.so.18 libwind.so.0 libheimbase.so.1 libhx509.so.5 libicuuc.so.55 libicudata.so.55 libboost_filesystem.so.1.71.0 lib/ubuntu1604-x64/libloaders.a lib/ubuntu1604-x64/librenderer.a lib/ubuntu1604-x64/librtcore.a lib/ubuntu1604-x64/libsys.a lib/ubuntu1604-x64/libimage.a lib/ubuntu1604-x64/liblexers.a -o game_script

